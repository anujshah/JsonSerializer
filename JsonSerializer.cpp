#include <iostream>
#include <fstream>

using namespace std;

string readFile(char *fileName)
{
	ifstream infile;
	infile.open(fileName);

	string line;
	string data;
	while(getline(infile,line))
		data+=line;
	return(data);
	infile.close();
}

void writeFile(char *fileName,string dest)
{
	ofstream outfile;
	outfile.open(fileName);
	outfile<<dest;
	outfile.close();
}


class JsonSerializer
{
	string s;
	string dest;
	int index;
public:
	JsonSerializer()
	{
		index=0;
	}
	void Serialize(char *i,char *o)
	{	
	s=readFile(i);
	S();
	writeFile(o,dest);
	
	}
	
	void ignore()
	{
		while(s[index]==' ' || s[index]=='\t')
			index++;
	}

	void S();
	void Data();
	void DataDash();
	void Pair();
	void Value();
	void Array();
	void String();

};

void JsonSerializer::S()
{
	ignore();
	if(s[index]=='{')
	{
		dest+=s[index++];
		Data();
		ignore();
		if(s[index]=='}')
		{
			dest+=s[index++];
			
		}
	}
}

void JsonSerializer::Data()
{
	
	Pair();
	DataDash();
}

void JsonSerializer::DataDash()
{
	ignore();
	if(s[index]==',')
	{
		dest+=s[index++];
		Pair();
		DataDash();
	}
}

void JsonSerializer::Pair()
{
	String();
	ignore();


	if(s[index]==':')
	{

		dest+=s[index++];

		Value();
	}
	
}

void JsonSerializer::String()
{
	ignore();
	if(s[index]=='\"')
	{	
		dest+=s[index++];
		while(s[index]!='\"')
		{
			dest+=s[index++];
		}
		dest+=s[index++];
	}
	else if(s[index]=='\'')
	{
		dest+='\"';
		index++;
		while(s[index]!='\'')
		{
			dest+=s[index++];
		}
		dest+='\"';
		index++;
	}
}

void JsonSerializer::Value()
{
	ignore();
	if(s[index]=='[')
	{
		dest+=s[index++];
		Array();
		//cout<<s[index];
		ignore();
		if(s[index]==']')
			dest+=s[index++];
	}
	else if(s[index]=='{')
		S();
	else
	{
		String();
	}
	
}

void JsonSerializer::Array()
{
	ignore();
	String();
	if(s[index]==',')
	{
		dest+=s[index++];
		String();
		Array();
	}
	else
		String();
}


int main(int argc, char *argv[])
{
	char *ip,*op;
	if(argc!=3)
	{
		cout<<"ERROR: USAGE ./objectfilename inputfilename.json outputfilename.json"
	}
	ip=argv[1];
	op=argv[2];

	JsonSerializer serial;
	serial.Serialize(ip,op);
	return 0;
}



